require 'elasticsearch'

# Uses elasticsearch as persistent storage. Note that symbols are
# stored as strings in ES and need to be parsed
module Storage

  def Storage.destroy
    client = Elasticsearch::Client.new log: true
    if client.indices.exists :index => 'osm'
      client.delete_by_query :index => 'osm', :type => ['osm','relation'], :body => {}
    end
    sleep 3
    client.index  index: 'osm', type: 'osm', id: 0, body: {}
  end

  def Storage.put(name,id,rec)
    client = Elasticsearch::Client.new log: true
    rec[:element] = name
    client.index  index: 'osm', type: 'osm', id: id.to_i, body: rec
  end

  def Storage.has?(name,id)
    client = Elasticsearch::Client.new log: true
    res = client.search index: 'osm', body: {"query":{"match":{ id: id }}}
    hits = res["hits"]
    return hits["total"] > 0
  end

  def Storage.get(name,id)
    symbolize = lambda { |obj|
      return obj.reduce({}) do |memo, (k, v)|
        memo.tap { |m| m[k.to_sym] = symbolize.call(v) }
      end if obj.is_a? Hash

      return obj.reduce([]) do |memo, v|
        memo << symbolize.call(v); memo
      end if obj.is_a? Array

      obj
    }
    client = Elasticsearch::Client.new log: true
    res = client.search index: 'osm', body: {"query":{"match":{ id: id }}}
    hits = res["hits"]
    rec = hits["hits"].last["_source"]
    symbolize.call(rec)
  end
end
