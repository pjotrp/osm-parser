require 'ox'

# Main elements are :node, :way: :relation. They can all have :tag inside.
# :way (poly-line) inside also find :member, :nd (pointing to :node)
# :relation (links between elements) also :member (pointing to :way)
#
# Fortunately there appear to be no deeper levels.
#
#    element (attributes)-> subelements[] (subattributes)
#
# Essentially yields:

=begin
[:node, {:list=>[], :id=>"31091562", :lat=>"51.5853309",
:lon=>"0.1897523", :version=>"1", :timestamp=>"2007-09-29T11:38:54Z",
:changeset=>"536435", :uid=>"7274", :user=>"nickthecoder"}]

[:way, {:list=>[[:nd, {:ref=>"13794394"}], [:nd, {:ref=>"13794399"}],
[:nd, {:ref=>"3368211816"}], [:nd, {:ref=>"13794400"}], [:nd,
{:ref=>"13794401"}], [:nd, {:ref=>"13794402"}], [:nd,
{:ref=>"13794403"}], [:nd, {:ref=>"13794405"}], [:nd,
{:ref=>"13794420"}], [:nd, {:ref=>"13794414"}], [:tag, {:k=>"highway",
:v=>"residential"}], [:tag, {:k=>"maxspeed", :v=>"20 mph"}], [:tag,
{:k=>"name", :v=>"Settrington Road"}], [:tag, {:k=>"source:maxspeed",
:v=>"THE HAMMERSMITH AND FULHAM (20 MPH SPEED LIMIT) EXPERIMENTAL
TRAFFIC ORDER 2016"}], [:tag, {:k=>"surface", :v=>"asphalt"}]],
:id=>"2953295", :visible=>"true", :version=>"4",
:changeset=>"49170862", :timestamp=>"2017-06-01T17:01:00Z",
:user=>"KeepItSimpleJim", :uid=>"16279"}]

[:relation, {:list=>[[:member, {:type=>"way", :ref=>"3997465",
:role=>""}], [:member, {:type=>"way", :ref=>"572513020", :role=>""}],
[:member, {:type=>"way", :ref=>"4239637", :role=>""}], [:member,
{:type=>"way", :ref=>"572511269", :role=>"forward"}], [:member,
{:type=>"way", :ref=>"572511272", :role=>"forward"}], [:member,
{:type=>"way", :ref=>"572511270", :role=>""}], [:member,
{:type=>"way", :ref=>"74175769", :role=>""}], [:member, {:type=>"way",
:ref=>"8136404", :role=>""}], [:member, {:type=>"way",
:ref=>"8136403", :role=>""}], [:member, {:type=>"way",
:ref=>"8136406", :role=>""}], [:member, {:type=>"way",
:ref=>"5000431", :role=>""}], [:tag, {:k=>"network", :v=>"lcn"}],
[:tag, {:k=>"ref", :v=>"LCN"}], [:tag, {:k=>"route", :v=>"bicycle"}],
[:tag, {:k=>"type", :v=>"route"}]], :id=>"8141708", :visible=>"true",
:version=>"1", :changeset=>"57475865",
:timestamp=>"2018-03-24T06:31:53Z", :user=>"MacLondon",
:uid=>"322039"}]
=end

# See also http://www.ohler.com/ox/Ox/Sax.html

class OSMParser < ::Ox::Sax
  attr_reader :result
  def initialize func
    @func = func
    @result = []
  end

  def start_element(name)
    return if name == :osm || name == :bounds
    if [:node, :way, :relation].include?(name)
      # raise "Unexpected element name #{@elm_name}" if @elm_name
      @elm_name = name
      @elm_attrs = { list: [] }
    else
      raise "Unexpected sub element name #{@sub_name}" if @sub_name
      @sub_name = name
      @sub_attrs = {}
    end
  end

  def end_element(name)
    return if name == :osm || name == :bounds
    if [:node, :way, :relation].include?(name)
      # close main element
      rec = @func.call(@elm_name, @elm_attrs)
      @result.push(rec) if rec
      @elm_name = nil
    else
      @elm_attrs[:list].push({@sub_name => @sub_attrs})
      @sub_name = nil
    end
  end

  def attr(name, value)
    if @sub_name
      @sub_attrs[name] = value
    else
      @elm_attrs[name] = value if @elm_attrs
    end
  end

  def text(value)
    raise "Not supposed to be here!"
  end
end
