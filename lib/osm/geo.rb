module GEO

  def GEO::distance(p1,p2)
    return 0.0 if p1 == nil or p2 == nil
    lat1,lon1 = p1
    lat2,lon2 = p2
    p = 0.017453292519943295     #Pi/180
    a = 0.5 - Math.cos((lat2 - lat1) * p)/2 + Math.cos(lat1 * p) * Math.cos(lat2 * p) * (1 - Math.cos((lon2 - lon1) * p)) / 2
    return 12742 * Math.asin(Math.sqrt(a))
  end

end
