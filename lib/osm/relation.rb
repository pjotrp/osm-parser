# A relation is one of the core data elements that consists of one or
# more tags and also an ordered list of one or more nodes, ways and/or
# relations as members which is used to define logical or geographic
# relationships between other elements. A member of a relation can
# optionally have a role which describes the part that a particular
# feature plays within a relation.
#
# See https://wiki.openstreetmap.org/wiki/Relation

require 'osm-parser'
require 'osm/storage'

module OSMRelation

  include Storage

  # Get a subset of items from a relation, e.g.
  #
  # refs = OSMRelation::get_relations(fn, :relation, '286535', :member,
  #                                 lambda { |member| member[:ref] if member[:type] == "relation" })

  def self.get_id(fn,element_name,id,sub_element = nil,filter = nil)
    """Fetch members"""

    filter_list = lambda { |rec|
      return rec[:list] if sub_element == nil and filter == nil
      result = []
      rec[:list].each do |item|
        item.each do |name,attrs|
          # p ["HERE",name,attrs]
          next if sub_element and name != sub_element
          if filter
            res = filter.call(attrs)
            result.push res if res
          else
            result.push attrs
          end
        end
      end
      result
    }

    get_info = lambda { |name, rec|
      # Every time a record is parsed this lambda is called
      if name == element_name and rec[:id] == id
        Storage.put(element_name,id,rec)
        return filter_list.call(rec)
      end
      nil
    }

    if Storage.has?(element_name,id)
      p "CACHE HIT!"
      return filter_list.call(Storage.get(element_name,id))
      # return [element_name,id,Storage.get(element_name,id)]
    end
    handler = OSMParser.new(get_info)
    print("Parsing OSM #{fn}:#{element_name}:#{id}\n")
    Ox.sax_parse(handler, IO.open(IO.sysopen(fn)))
    handler.result.flatten
  end

  def self.get_ids(fn,element_name,ids,sub_element = nil,filter = nil,aggregate = nil)
    """Fetch members"""

    filter_list = lambda { |rec|
      return rec[:list] if sub_element == nil and filter == nil
      result = []
      rec[:list].each do |item|
        item.each do |name,attrs|
          next if sub_element and name != sub_element
          if filter
            res = filter.call(attrs)
            result.push res if res
          else
            result.push attrs
          end
        end
      end
      if aggregate
        aggregate.call(rec,result)
      else
        result
      end
    }

    get_info = lambda { |name, rec|
      # Every time a record is parsed this lambda is called
      id = rec[:id]
      if name == element_name and ids.include?(id)
        return filter_list.call(rec)
      end
      nil
    }

    handler = OSMParser.new(get_info)
    print("Parsing OSM #{fn}:#{element_name}:#{ids}\n")
    Ox.sax_parse(handler, IO.open(IO.sysopen(fn)))
    handler.result
  end

  def self.get_id_attrs(fn, element_name, id, filter)
    """Fetch all attributes"""
    get_info = lambda { |name, rec|
      # Every time a record is parsed this lambda is called
      if name == element_name and rec[:id] == id
        Storage.put(element_name,id,rec)
        return filter.call(rec)
      end
      nil
    }

    if Storage.has?(element_name,id)
      return filter.call(Storage.get(element_name,id))
    end
    handler = OSMParser.new(get_info)
    print("Parsing OSM #{fn}:#{element_name}:#{id}\n")
    Ox.sax_parse(handler, IO.open(IO.sysopen(fn)))
    route = handler.result
    list = self.get(fn,element_name,id,nil,nil)
    # p list
    list
  end

  def self.get_ids_attrs(fn, element_name, ids, filter)
    """Fetch all attributes"""
    get_info = lambda { |name, rec|
      id = rec[:id]
      if name == element_name and ids.include?(id)
        # p [name,id,rec]
        return [id,filter.call(rec)]
      end
      nil
    }

    handler = OSMParser.new(get_info)
    print("Parsing OSM #{fn}:#{element_name}:#{ids}\n")
    Ox.sax_parse(handler, IO.open(IO.sysopen(fn)))
    # switch to original order
    # decompose into hash of ids pointing to nodes again
    list = {}
    handler.result.each do | h |
      list[h[0]] = h[1]
    end
    ids.map { |id| list[id] }
  end

end
